function carte() {
    let map = L.map('map', {
        minZoom: 15,
        maxZoom: 19
    });
    map.setView([48.584293883712505, 7.7447360381375], 15);
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 19
    }).addTo(map);

    return map;
}

/*function fond_chemin(map) {
    for (let i = 0; i < routes.length; i++) {
        for (let j = 0; j < routes[i].length - 1; j++) {
            let source = [arrets[arrets.map(function (e) {
                    return e.id;
                }).indexOf(routes[i][j])].latitude,
                arrets[arrets.map(function (e) {
                    return e.id;
                }).indexOf(routes[i][j])].longitude
            ];
            let destination = [arrets[arrets.map(function (e) {
                return e.id;
            }).indexOf(routes[i][j + 1])].latitude, arrets[arrets.map(function (e) {
                return e.id;
            }).indexOf(routes[i][j + 1])].longitude];
            let fond_chemin = L.polyline([source, destination], {
                color: 'black',
                weight: largeur
            }).addTo(map);
        }
    }
}*/

function chemins(map, arrets, routes) {
    let couleur = ["#EE1D23", "#00AEEF", "#F7931D", "#00AB4D", "#8781BD", "#8BC63E"];
    for (let i = 0; i < routes.length; i++) {
        for (let j = 0; j < routes[i].length - 1; j++) {
            let source = [arrets[arrets.map(function(e) {
                    return e.id;
                }).indexOf(routes[i][j])].latitude,
                arrets[arrets.map(function(e) {
                    return e.id;
                }).indexOf(routes[i][j])].longitude
            ];

            let destination = [arrets[arrets.map(function(e) {
                return e.id;
            }).indexOf(routes[i][j + 1])].latitude, arrets[arrets.map(function(e) {
                return e.id;
            }).indexOf(routes[i][j + 1])].longitude];

            let route = L.polyline([source, destination], {
                color: couleur[i],
                offset: i,
                weight: largeur / 6
            }).addTo(map);
        }
    }
}

function arret(map, arrets, routes) {
    let term = [];
    for (let i = 0; i < routes.length; i++) {
        for (let j = 0; j < routes[i].length; j++) {
            if (j == 0 || j == routes[i].length - 1) {
                term.push(routes[i][j]);
            }
        }
    }
    for (let i = 0; i < arrets.length; i++) {
        let arret = L.circleMarker([arrets[i].latitude, arrets[i].longitude], {
            color: 'black',
            fillColor: 'white',
            fillOpacity: 1,
            radius: largeur,
            weight: largeur / 5,
            opacity: 1
        }).addTo(map);
        if (term.includes(arrets[i].id)) {
            let terminus = L.circleMarker([arrets[i].latitude, arrets[i].longitude], {
                color: 'black',
                fillColor: 'black',
                fillOpacity: 1,
                radius: largeur / 4,
                weight: largeur / 5,
                opacity: 1
            }).addTo(map);
            terminus.bindPopup("<p>" + arrets[i].nom + "</p><p>" + arrets[i].id + "</p>");
        }
        arret.bindPopup("<p>" + arrets[i].nom + "</p><p>" + arrets[i].id + "</p>");
    }
}

async function importer(url) {
    try {
        const response = await fetch(url, { method: "GET" });
        const data = await response.json();
        return data;
    } catch (err) {
        console.error(err);
    }
}

let url = "http://localhost:8080/temps-reel/";

let prom_arrets = importer(url + "arrets/");
let prom_routes = importer(url + "routes/");

let carto = carte();
//Variable définissant la largeur du chemin
let largeur = 15;

Promise.all([prom_arrets, prom_routes]).then(([arrets, routes]) => {
    chemins(carto, arrets, routes);
    arret(carto, arrets, routes);
});